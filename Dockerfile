FROM python:3.9.0-alpine

ENV PYTHONUNBUFFERED 1

ADD . /shipment_backend_api
WORKDIR /shipment_backend_api
RUN pip install -r requirements.txt

EXPOSE 8000
CMD python manage.py makemigrations && python manage.py migrate && gunicorn shipment_backend.wsgi --bind 0.0.0.0:8000
