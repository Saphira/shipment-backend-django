# Shipment Management Backend
This is a test application with Django REST framework as Backend for a simple Shipment list
 
### Includes
* Django
* Django REST framework 
* Gunicorn

## Prerequisites
Before getting started you should have the following installed and running:
- [X] Python 3 - [instructions](https://wiki.python.org/moin/BeginnersGuide)

# Setup
* Create a virtual env: `$ virtualenv -p python3 <path_to_environment>`
* Activate the virtual env: `$ source <path_to_environment> /bin/activate`
* Install Python dependencies: `$ pip install -r requirements.txt`
* Migrate: `$ python manage.py migrate`
* Load Data into the database: `$ python manage.py loaddata shipment`

# Run the application
* Running development server: `$ python manage.py runserver`
* The API will be available in: http://127.0.0.1:8000/api/v1

# Test
* Tests: `python manage.py test`

# Build application with docker
* Build docker image: `docker build -t shipment-backend:0.1.0`
* Run Frontend container: `docker run -d 8585:8000 shipment-backend:0.1.0`
* The API will be available in: http://localhost:8085/api/v1 