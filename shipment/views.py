from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from .serializers import *


class ShipmentViewSet(viewsets.ModelViewSet):
    """
   This view presents a list of all the shipments in the database and allows new shipments to be created.

   Note that the shipments are paginated to a maximum of 10 per page
   """
    queryset = Shipment.objects.all()
    serializer_class = ShipmentSerializer


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'shipments': reverse('shipment-list', request=request, format=format)
    })
