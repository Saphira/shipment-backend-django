from django.db import models


class Shipment(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    delivery_date = models.DateTimeField(null=True, blank=True)
    tracking_number = models.CharField(unique=True, max_length=20)
    location_origin = models.CharField(max_length=50)
    location_destination = models.CharField(max_length=50)

