from rest_framework import serializers
from .models import *


class ShipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = '__all__'

    def validate(self, data):
        if data['location_origin'] == data['location_destination']:
            raise serializers.ValidationError("Origin must be different from Destination")
        return data
