from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Shipment
from .serializers import ShipmentSerializer


class ShipmentModelTestCases(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.tracking_number = 'TN00001'
        cls.delivery_date = '2020-11-15T17:49:52.832Z'
        cls.location_origin = 'Tallinn'
        cls.location_destination = 'Tartu'
        cls.shipment = Shipment(
            tracking_number=cls.tracking_number,
            delivery_date=cls.delivery_date,
            location_origin=cls.location_origin,
            location_destination=cls.location_destination)

    @classmethod
    def tearDownClass(cls):
        pass

    def test_model_can_create_a_shipment(self):
        old_count = Shipment.objects.count()
        self.shipment.save()
        new_count = Shipment.objects.count()
        self.assertNotEqual(old_count, new_count)


class ViewTestCases(APITestCase):
    @classmethod
    def setUpClass(cls):
        cls.valid_payload = {
            'tracking_number': 'TN00101',
            'delivery_date': '2020-11-15T17:49:52.832Z',
            'location_origin': 'Tallinn',
            'location_destination': 'Tartu'
        }
        cls.invalid_payload = {
            'tracking_number': '',
            'delivery_date': None,
            'location_origin': '',
            'location_destination': ''
        }
        cls.invalid_location_origin_payload = {
            'tracking_number': 'TN00104',
            'delivery_date': None,
            'location_origin': 'Tallinn',
            'location_destination': 'Tallinn'
        }
        cls.shipment1 = Shipment.objects.create(
            tracking_number='TN00102',
            delivery_date='2020-11-15T17:49:52.832Z',
            location_origin='Tartu',
            location_destination='Tallinn'
        )
        cls.shipment2 = Shipment.objects.create(
            tracking_number='TN00103',
            delivery_date='2020-11-16T17:49:52.832Z',
            location_origin='Tartu',
            location_destination='Tallinn'
        )
        cls.url = reverse('shipment-list')

    @classmethod
    def tearDownClass(cls):
        pass

    def test_api_can_create_a_shipment_with_valid_data(self):
        response = self.client.post(self.url, self.valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_api_does_not_create_shipment_with_invalid_data(self):
        response = self.client.post(self.url, self.invalid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_api_does_not_create_shipment_with_origin_equal_to_location(self):
        response = self.client.post(self.url, self.invalid_location_origin_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        message = response.json()
        self.assertEqual(message['non_field_errors'], ['Origin must be different from Destination'])

    def test_api_can_update_a_shipment_with_valid_data(self):
        response = self.client.put(f"{self.url}{self.shipment1.id}/", self.valid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_does_not_update_a_shipment_with_invalid_data(self):
        response = self.client.put(f"{self.url}{self.shipment1.id}/", self.invalid_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_api_does_not_update_a_shipment_with_origin_equal_to_location(self):
        response = self.client.put \
            (f"{self.url}{self.shipment1.id}/", self.invalid_location_origin_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        message = response.json()
        self.assertEqual(message['non_field_errors'], ['Origin must be different from Destination'])

    def test_api_can_get_all_shipments(self):
        response = self.client.get(self.url)
        shipments = Shipment.objects.all()
        serializer = ShipmentSerializer(shipments, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_can_get_a_valid_shipment(self):
        response = self.client.get(f"{self.url}{self.shipment1.id}/")
        ship1 = Shipment.objects.get(pk=self.shipment1.pk)
        serializer = ShipmentSerializer(ship1)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_get_invalid_shipment_response_404(self):
        response = self.client.get(f"{self.url}{30}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_api_can_delete_a_valid_shipment(self):
        response = self.client.delete(f"{self.url}{self.shipment1.id}/")
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_delete_invalid_shipment_response_404(self):
        response = self.client.delete(f"{self.url}{30}/")
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)
